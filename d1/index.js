console.log("Hello World");

//Array Methods
//JS havs built-in function and methods for arrays. This allows us to manipulate and access our array items.

//Mutator Methods

/*
	-Mutator Methods are functions that "mutate" or change our arrays after they are created
	-These methods manipulate the original array performing various tast such as adding and removing elements
*/

	let fruits = ["apple", "orange", "kiwi", "Dragon Fruit"];
	//push()
		/*
			-adds an element in the end of an array AND returns the array's length

			Syntax:
				arrayName.push()
		*/

	console.log("current array");
	console.log(fruits);
	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength)
	console.log("Mutated array from push method");
	console.log(fruits);//Mango is added at the end of the array

	fruits.push("Avocado", "Guava");
	console.log("Mutated array from push method");
	console.log(fruits);

	//pop()
	/*
		Removes the last element in an array AND returns the remove element

		Syntax:
			arrayName.pop();
	*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);//"Guava"
	console.log("Mutated array from pop");
	console.log(fruits);


	let ghostFighters = ["Eugene", "Dennis", "Alfred", "Taguro"];

	function removeFriend(){
		ghostFighters.pop()
	};

	removeFriend();
	console.log(ghostFighters);

	//unshift()
	/*
		-adds one or more elements at the beginning of an array

		Syntax:
			arrayName.unshift(elementA);
			arrayName.unshift(elementA, elementB);
	*/

	fruits.unshift("Lime", "Banana");
	console.log("Mutated Array from unshift method");
	console.log(fruits);

	//shift()
	/*
		removes an element at the beginning of an array AND returns the removed elements

		Syntax:
			arrayName.shift();
	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);//Lime
	console.log(fruits);

	//splice
	/*
		simultaneously removes elements from a specified index number and adds elements

		Syntax:
			arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);
	*/

	fruits.splice(1, 2, "Lime", "Cherry");
	console.log("Mutated array from splice method");
	console.log(fruits);

	//sort
	/*
		Rearranges the array elements in alphanumeric order

		Syntax:
			arrayName.sort();
	*/

	fruits.sort();
	console.log("Mutated array from sort method");
	console.log(fruits);

	//reverse
	/*
		Reverse the order of array elements

		Syntax:
			arrayName.reverse();
	*/

	fruits.reverse();
	console.log("Mutated array from reverse method");
	console.log(fruits);


//Non-Mutator Methods
/*
	None-Mutator methods are functions that do not modify or change an array after they are created
	-these methods do not manipulate the original array performing various tasks such as returning elements from an array and also combining arrays and printing the output
*/

let countries =["US", "PH" , "CAN", "SG", "TH", "PH", "FR", "DE"];

//index()
/*
	returns the index number of the first matching element found in an array
	-if no match is found, the result is -1
	-the search process will be done from first element proceeding to the last element

	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log("Results of indexOf method " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Results of indexOf method " + invalidCountry);

//lastIndexOf()
/*
	Returns the index number of the last matching element found in our array
	-the search process will be done from the last element proceeding to the first element

	Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndex = countries.lastIndexOf("PH", 1);
console.log("Result of lastIndexOf method " + lastIndex);

//slice()
/*
		portions/slices elements from an array AND returns a new array

		Syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
*/

let slicedArrayA = countries.slice(2);
console.log("Results from slice method");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log("Results from slice method");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Results from slice method");
console.log(slicedArrayC);

//toString()
/*
	Retruns an array as a string, seperated by comas

	Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString()
console.log("Results from toString method");
console.log(stringArray);

//concat()
/*
	Combines two array and returns the combined results

	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let taskArrayA = ["drink HTML", "Eat JS"];
let taskArrayB = ["Inhale CSS", "Breathe SASS"];
let taskArrayC = ["Get Git", "Be Node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Results from concat method");
console.log(tasks);

//combine multiple arrays

console.log("Results from concat method");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

//combining arrays with elements
let combinedTasks = taskArrayA.concat("Smell Express", "Throw react");
console.log(combinedTasks);

//join()
/*
	Returns an array as a string seperated by specified seperator

	Syntax:
	arrayName.join("seperatorString";)
*/

let users = ["John", "Jane", "Joe", "Robert", "Nej"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));

//Iteration Method

/*
	Iteration method are loops designed to perform repetitive tasks on arrays
	Iteration methods loops all over items in an array
	Useful in manipulating array data resulting in complex tasks
*/

//forEach
/*
	Similar to a for lopp that on each array element
	- for each item in the array, the anonymous function passed in the forEach()
	- The anonymous function is able to recieve the current item being iterated or loop over by assigning a parameter
	-variable names for arrays are normally written in the plural form of the data stored in an array
	-It's common practice to use th singular form of the array content for parameter names used in arrayloops
	-forEach() does NOT return anything

	Syntax:
		arrayName.forEach(function(individualElement){
			statement
		})
*/

	allTasks.forEach(function(task){
		console.log(task);
	});

	ghostFighters.forEach(function(fighter){
		console.log(fighter)
	});

	//Using foeEach with conditional statements

	/*
		It's good practice to print the current element in the console when working with array iteration methods to have an idea what information is being worked on for each iteration of the loop

		creating seperate variable to store results of an array iteration
		iterration methods are also good practice to avoid confusion by modifying the original array

		Mastering loops and array allow us developers to perform a wide range of features that help in data management and analysis
	*/

	let filteredTasks = [];


	allTasks.forEach(function(task){
		console.log(tasks);
		if(task.length > 10){
			console.log(task)
			filteredTasks.push(task)
		}
	});

	console.log("Results of filtered tasks: ");
	console.log(filteredTasks);

//map()
/*
	Iterates on each element AND returns new array with different values depending on the result of the function's operation

	Syntax:
		let/const resultArray = arrayName.map(function(indivElement))
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
});

console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);

//map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
});

console.log(numberForEach);//undefined

//every()
/*
	Checks if all elements in an array meet the given conditions
	This is useful for validating data stored in arrays especially when dealing with large ammounts of data
	-returns true if all elements meet the condition, otherwise false

	Syntax:
		let/const resultArray = arrayName.every(function(indivElement){
			return expression/condition
		});
*/

let allValid = numbers.every(function(number){
	return (number < 3)
});
console.log("Result of every method: ");
console.log(allValid);//false

//some()
/*
	check if at least one element in the array meets the given condition
	-returns a true value if one element meets the condition and false otherwise

	Syntax:
		let/const resultArray = arrayName.some(function(indivElement){
			return expression/condition
		});
*/

let someValid = numbers.some(function(number){
	return (number <2)
});

console.log("The result of some method: ");
console.log(someValid);//true

//combining the returned result from the every/some method may be used in other statement to perform consecutive results

if(someValid){
	console.log("Some numbers in the array are greater than 2")
};

//filter
/*
	Returns a new arrays that contains elements which meets the given condition
	-returns an empty array if no elements were found

	Syntax:
		let/const resultArray = arrayName.filter(function(indivElement){
			return expression/condition
		});
*/

let filterValid = numbers.filter(function(number){
	return (number<3)
});

console.log("Results of filter method: ");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0)
});
console.log("Results of filter method: ");
console.log(nothingFound);

//Filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	//console.log(number);

	if(number < 3){
		filteredNumbers.push(number);
	};
})

console.log("Result of filter method: for each");
console.log(filteredNumbers);

//includes()
/*
	checks if the argument passed can be found in the array
	-it returns a boolean which can also be saved in a variable
	-returns true if the arguement is found in the array
	-returns false if it is not

	Syntax:
		arrayName.include(<argumentToFind>)
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
let productFound = products.includes("Mouse");
console.log(productFound);

let productNotFound = products.includes("Headset");
console.log(productNotFound);

//Method Chaining
	//Methods can be chained using them one after another
	//the result of the first method is used on the second method until all "chained" methods have been resolved

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	});

	console.log(filteredProducts);

	let contacts =["Ash"]

	function addTrainer(trainer){
		if (contacts.includes(trainer) === true){
			alert("Already added in Match Call")
		}else{
			contacts.push(trainer);
			alert("Registered!")
			console.log(contacts)
		}
	};

//reduce
/*
	evaluates elements from left to right and returns/reduce the array into a single value

	Syntax:
		let/const resultArray = arrayName.reduce(function(accumulator,currentValue){
			return expression/operation
		});

	"accumulator" parameter in the function stores the result for every iteration of the loop
	"currentValue" is the current/ next element of the array that is evaluated in each iteration of the loop
*/

console.log(numbers);
let iteration = 0;
let iterationStr = 0;

let reducedArray = numbers.reduce(function(x,y){
	console.warn("current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("current value: " + y);

	return x + y;
})

console.log("Result of reduced method: " + reducedArray);

let list = ["Hello", "Again", "World"];
let reducedJoin = list.reduce(function(x,y){
			console.warn("current iteration: " + ++iterationStr);
	console.log("accumulator: " + x);
	console.log("current value: " + y);

	return x + " " + y
});

console.log("Result of reduced method");
console.log(reducedJoin);